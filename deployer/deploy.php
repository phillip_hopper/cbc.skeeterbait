<?php

namespace Deployer;

use Throwable;

require 'recipe/common.php';

// Project name
set('application', 'carsen');

// Project repository
set('repository', 'git@bitbucket.org:phillip_hopper/cbc.skeeterbait.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Shared files/dirs between deploys
set('shared_files', []);
set('shared_dirs', ['logs', 'cache', 'config', '.well-known']);
//set('clear_paths', ['cache/*']);

// Writable dirs by web server
set('writable_dirs', ['cache', 'logs', '.well-known']);

//set('composer_options', 'install --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader');
//set('npm_options', 'install --production');

// Hosts
set('default_stage', 'test');
inventory('hosts.yml');


// Tasks
desc('Add the ssh key');
task('deploy:ssh_add', function () {
    run('ssh-add ~/.ssh/id_rsa');
});

task('deploy:well_known_symlink', function () {

    // create link to the static resources in the template
    run("ln -sfn {{deploy_path}}/shared/.well-known/ {{release_path}}/public/.well-known");

})->desc('Creating symlink to the .well-known shared directory.');

task('deploy:npm', function () {
    run("cd {{release_path}} && npm {{npm_options}}");
})->desc('Install the nodejs packages needed to compile sass and typescript files.');

desc('Update code (extended)');
task('deploy:update_code_ex', function () {
    $repository = trim(get('repository'));
    $branch = get('branch');
    $git = get('bin/git');
    $gitCache = get('git_cache');
    $recursive = get('git_recursive', true) ? '--recursive' : '';
    $quiet = isQuiet() ? '-q' : '';
    $depth = $gitCache ? '' : '--depth 1';
    $options = [
        'tty' => get('git_tty', false),
    ];

    $at = '';
    if (!empty($branch)) {
        $at = "-b $branch";
    }

    // If option `tag` is set
    if (input()->hasOption('tag')) {
        $tag = input()->getOption('tag');
        if (!empty($tag)) {
            $at = "-b $tag";
        }
    }

    // If option `tag` is not set and option `revision` is set
    if (empty($tag) && input()->hasOption('revision')) {
        $revision = input()->getOption('revision');
        if (!empty($revision)) {
            $depth = '';
        }
    }

    // Enter deploy_path if present
    if (has('deploy_path')) {
        cd('{{deploy_path}}');
    }

    if ($gitCache && has('previous_release')) {
        try {
            // NB: 19 SEP 2019: Phil Hopper // run("$git clone $at $recursive $quiet --reference {{previous_release}} --dissociate $repository  {{release_path}} 2>&1", $options);
            run("ssh-agent sh -c 'ssh-add ~/.ssh/id_rsa; $git clone $at $recursive $quiet --reference {{previous_release}} --dissociate $repository  {{release_path}} 2>&1'", $options);
        } catch (Throwable $exception) {
            // If {{deploy_path}}/releases/{$releases[1]} has a failed git clone, is empty, shallow etc, git would throw error and give up. So we're forcing it to act without reference in this situation
            // NB: 19 SEP 2019: Phil Hopper // run("$git clone $at $recursive $quiet $repository {{release_path}} 2>&1", $options);
            run("ssh-agent sh -c 'ssh-add ~/.ssh/id_rsa; $git clone $at $recursive $quiet $repository {{release_path}} 2>&1'", $options);
        }
    } else {
        // if we're using git cache this would be identical to above code in catch - full clone. If not, it would create shallow clone.
        // NB: 19 SEP 2019: Phil Hopper // run("$git clone $at $depth $recursive $quiet $repository {{release_path}} 2>&1", $options);
        run("ssh-agent sh -c 'ssh-add ~/.ssh/id_rsa; $git clone $at $depth $recursive $quiet $repository {{release_path}} 2>&1'", $options);
    }

    if (!empty($revision)) {
        run("cd {{release_path}} && $git checkout $revision");
    }
});


desc('Deploy your project');
task('deploy', [
    'deploy:info',
//    'deploy:ssh_add',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code_ex',
    'deploy:shared',
    'deploy:writable',
    'deploy:well_known_symlink',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
