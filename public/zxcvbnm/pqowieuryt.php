<?php

/**
 *
 * @param IZoomSettings $zoom_settings
 * @param IClassroomSettings $classroom_settings
 *
 * @return string
 */
function generate_signature($zoom_settings, $classroom_settings)
{
    $role = 0;
    $time = (time()) * 1000; // time in milliseconds (or close enough)

    // data: api_key . meeting_number . milliseconds . role
    $data = base64_encode($zoom_settings->api_key . $classroom_settings->zoom . $time . $role);
    $hash = hash_hmac('sha256', $data, $zoom_settings->api_secret, true);
    $_sig = $zoom_settings->api_key . '.' . $classroom_settings->zoom . '.' . $time . '.' . $role . '.' . base64_encode($hash);

    // return signature, url safe base64 encoded
    return rtrim(strtr(base64_encode($_sig), '+/', '-_'), '=');
}

$project_dir = dirname(dirname(__DIR__));
$zoom_file = $project_dir . '/config/zoom.json';

/** @var IZoomSettings $zoom_settings */
$zoom_settings = json_decode(file_get_contents($zoom_file));

$classroom_file = $project_dir . '/config/settings.json';

/** @var IClassroomSettings $classroom_settings */
$classroom_settings = json_decode(file_get_contents($classroom_file));

$result = [
    'signature' => generate_signature($zoom_settings, $classroom_settings),
    'meeting_id' => $classroom_settings->zoom,
    'api_key' => $zoom_settings->api_key,
    'password' => $classroom_settings->password
];

header('Content-Type: application/json');
echo json_encode($result);
