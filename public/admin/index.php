<?php

include_once dirname(dirname(__DIR__)) . '/app/app_start.php';

$classroom_file = CONFIG_DIR . DS . 'settings.json';

$zoom_id    = filter_input(INPUT_POST, 'zoom_id', FILTER_SANITIZE_STRING);
$zoom_pwd   = filter_input(INPUT_POST, 'zoom_pwd', FILTER_SANITIZE_STRING);
$nearpod_id = filter_input(INPUT_POST, 'nearpod_id', FILTER_SANITIZE_STRING);

if (!empty($zoom_id) && !empty($nearpod_id)) {

    // remove spaces and dashes from zoom meeting id
    $zoom_id = str_replace('-', '', str_replace(' ', '', $zoom_id));
    ClassroomSettings::SaveSettings($zoom_id, $zoom_pwd, $nearpod_id);

    header('Location: /');
    exit;
}


/** @var IClassroomSettings $classroom_settings */
$classroom_settings = json_decode(file_get_contents($classroom_file));

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Page</title>
    <style type="text/css">
        * {
            font-family: sans-serif;
        }
        table {
            margin: 20px auto;
        }
        td {
            padding: 4px 8px;
            font-size: 16px;
        }
        input[type=text], input[type=password] {
            padding: 6px 10px;
            font-size: 16px;
        }
        label {
            font-size: 16px;
        }
        button {
            font-size: 16px;
            padding: 8px 16px;
        }
    </style>
</head>
<body>
<h1 style="text-align: center">Classroom Settings</h1>
<form method="post">
    <table>
        <tr>
            <td><label for="zoom_id">Meeting ID</label></td>
            <td><input type="text" placeholder="Like 811192620" name="zoom_id" id="zoom_id" value="<?php echo $classroom_settings->zoom ?>"></td>
        </tr>
        <tr>
            <td><label for="zoom_pwd">Password</label></td>
            <td><input type="password" placeholder="Can be blank" name="zoom_pwd" id="zoom_pwd" value="<?php echo $classroom_settings->password ?>"></td>
        </tr>
        <tr>
            <td><label for="nearpod_id">Nearpod PIN</label></td>
            <td><input type="text" placeholder="Like PIJFO" name="nearpod_id" id="nearpod_id" value="<?php echo $classroom_settings->nearpod ?>"></td>
        </tr>
        <tr>
            <td></td>
            <td><button type="submit" value="submit">Submit</button></td>
        </tr>
    </table>
</form>
</body>
</html>
