<?php

include_once dirname(dirname(__DIR__)) . '/app/app_start.php';

/** @var IClassroomSettings $classroom_settings */
$classroom_settings = ClassroomSettings::GetSettings();

$child_name = filter_input(INPUT_POST, 'child_name', FILTER_SANITIZE_STRING);

if (empty($child_name)) {

    header('Location: /');
    exit;
}

$child_name = urlencode($child_name);
$pin = $classroom_settings->nearpod;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Classroom</title>
    <style type="text/css">
        div.nearpod {
            position: absolute;
            left: 0;
            top: 0;
            width: calc(100vw - 302px);
        }
        div.zoom {
            position: absolute;
            width: 300px;
            right: 0;
            top: 0;
        }
        iframe {
            border: 0;
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<body>
<div class="nearpod" id="nearpod">
    <iframe src="https://app.nearpod.com/presentation?pin=<?php echo $pin; ?>"></iframe>
</div>
<div class="zoom" id="zoom">
<!--    <iframe src="zoom.php?child_name=--><?php //echo $child_name; ?><!--"></iframe>-->
</div>

<script src='https://code.jquery.com/jquery-3.4.1.slim.min.js'></script>
<script src='https://meet.jit.si/external_api.js'></script>
<script type="text/javascript">

    function setHeight() {
        let h = document.documentElement.clientHeight;

        let n = document.getElementById('nearpod');
        n.style.maxHeight = h + 'px';
        n.style.height = h + 'px';

        let z = document.getElementById('zoom');
        z.style.maxHeight = h + 'px';
        z.style.height = h + 'px';
    }

    function joinJitsi() {

        const domain = 'meet.jit.si';
        const options = {
            roomName: '<?php echo $classroom_settings->zoom; ?>',
            width: '100%',
            height: '100%',
            parentNode: document.querySelector('#zoom')
        };
        const api = new JitsiMeetExternalAPI(domain, options);
        api.executeCommand('displayName', '<?php echo $child_name; ?>');
    }

    window.addEventListener('resize', setHeight);
    document.addEventListener('DOMContentLoaded', function() {

        setHeight();

        setTimeout(function() {
            joinJitsi();
        }, 3000);
    });
</script>
</body>
</html>
