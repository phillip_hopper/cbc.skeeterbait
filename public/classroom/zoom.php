<?php

include_once dirname(dirname(__DIR__)) . '/app/app_start.php';

$child_name = filter_input(INPUT_GET, 'child_name', FILTER_SANITIZE_STRING) ?: 'Unknown';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zoom Room</title>
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.5/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.5/css/react-select.css" />
</head>
<body>
<style type="text/css">
    #dialog-join .tab, #dialog-invite .tab {
        width: 260px;
    }
    #wc-footer .popmenu.audioAllowMenu.dropdown-menu {
        left: -86px;
    }

    #wc-footer .more-button__pop-menu.dropdown-menu {
        left: -100px;
    }

    .ReactModalPortal .ReactModal__Content.ReactModal__Content--after-open {
        max-width: 96vw;
    }

    .ReactModalPortal .ReactModal__Content.ReactModal__Content--after-open .zm-modal {
        min-width: 0;
        width: 96vw;
    }

    #wc-footer .popmenu.audioMenu.dropdown-menu {
        left: -190px;
    }

    /*.diaout-layer .dialog-ctn {*/
    /*    background-color: #000;*/
    /*    position: relative;*/
    /*    padding: 12px;*/
    /*}*/

    .diaout-layer .dialog-ctn .content {
        min-width: auto;
        max-width: 100%;
        width: 100%;
        margin: 50px 0 0;
    }

    .diaout-layer .dialog-ctn .content .presstip {
        margin-left: 0;
    }

</style>
<script src="https://source.zoom.us/1.7.5/lib/vendor/react.min.js"></script>
<script src="https://source.zoom.us/1.7.5/lib/vendor/react-dom.min.js"></script>
<script src="https://source.zoom.us/1.7.5/lib/vendor/redux.min.js"></script>
<script src="https://source.zoom.us/1.7.5/lib/vendor/redux-thunk.min.js"></script>
<script src="https://source.zoom.us/1.7.5/lib/vendor/jquery.min.js"></script>
<script src="https://source.zoom.us/1.7.5/lib/vendor/lodash.min.js"></script>
<script src="https://source.zoom.us/zoom-meeting-1.7.5.min.js"></script>

<script type="text/javascript">


    function joinMeeting() {

        console.log(JSON.stringify(ZoomMtg.checkSystemRequirements()));

        ZoomMtg.preLoadWasm();
        ZoomMtg.prepareJssdk();

        $.ajax({
            url: '/zxcvbnm/pqowieuryt.php',
            dataType: 'json',
            type: 'GET'
        }).done(function (data) {

            if (data === null || (typeof data !== 'object')) {
                alert('We apologize, something went wrong.');
                console.log(data);
                return;
            }

            if (!('signature' in data)) {
                alert('We apologize, something went wrong.');
                console.log(data);
                return;
            }

            ZoomMtg.init({
                leaveUrl: document.location.href,
                isSupportAV: true,
                disableInvite: true,
                disableRecord: true,
                success: function () {
                    ZoomMtg.join(
                        {
                            meetingNumber: data['meeting_id'],
                            userName: '<?php echo $child_name ?>',
                            signature: data['signature'],
                            apiKey: data['api_key'],
                            passWord: data['password'],
                            success: function(){
                                $('#nav-tool').hide();
                                console.log('join meeting success');
                            },
                            error: function(res) {
                                console.log(res);
                            }
                        }
                    );
                },
                error: function(res) {
                    console.log(res);
                }
            });
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        });
    }

    document.addEventListener('DOMContentLoaded', function() {
        setTimeout(joinMeeting, 3000);
    });

</script>
</body>
</html>
