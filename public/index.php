<?php


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login Page</title>
    <style type="text/css">
        * {
            font-family: sans-serif;
            font-size: 18px;
        }
        table {
            margin: 60px auto 0;
        }
        td {
            text-align: center;
            padding: 4px 8px;
        }
        input[type=text] {
            padding: 10px;
            width: 240px;
        }
        h1 {
            font-size: 2em;
        }
        h2 {
            font-size: 1.5em;
        }
    </style>
</head>
<body>
<h1 style="text-align: center">Child Sign-In</h1>
<h2 style="text-align: center">Covenant Baptist Church</h2>
<form method="post" action="/classroom/index.php">
    <table>
        <tr>
            <td><label for="child_name">Name of Child - First and Last</label></td>
        </tr>
        <tr>
            <td><input type="text" name="child_name" id="child_name"></td>
        </tr>
        <tr>
            <td><button type="submit" style="margin-top: 20px; padding: 8px 16px" onclick="saveName();">Continue</button></td>
        </tr>
    </table>
</form>
<script src="https://source.zoom.us/1.7.5/lib/vendor/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<!--suppress JSUnresolvedVariable -->
<script type="text/javascript">

    function saveName() {

        let n = document.getElementById('child_name').value;
        if (n) {
            let d = new Date();
            d.setDate(d.getDate() + 180);
            $.cookie('child_name',
                n,
                {expires: d}
            );
        }
    }

    document.addEventListener('DOMContentLoaded', function() {

        let n = $.cookie('child_name');
        if (n)
            document.getElementById('child_name').value = n;
    });
</script>
</body>
</html>
