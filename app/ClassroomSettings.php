<?php


class ClassroomSettings
{
    private static $classroom_file;

    /**
     * return IClassroomSettings
     */
    public static function GetSettings()
    {
        return json_decode(file_get_contents(self::ClassroomFile()));
    }

    public static function SaveSettings($zoom_id, $zoom_pwd, $nearpod_id)
    {
        $settings = ['zoom' => $zoom_id, 'password' => $zoom_pwd, 'nearpod' => $nearpod_id];
        file_put_contents(self::ClassroomFile(), json_encode($settings));
    }

    private static function ClassroomFile()
    {
        if (empty(self::$classroom_file))
            self::$classroom_file  = CONFIG_DIR . DS . 'settings.json';

        return self::$classroom_file;
    }
}
