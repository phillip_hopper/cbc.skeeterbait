<?php

/**
 * Interface IZoomSettings
 *
 * @property string api_key
 * @property string api_secret
 *
 */
interface IZoomSettings
{
}

/**
 * Interface IClassroomSettings
 *
 * @property string zoom
 * @property string password
 * @property string nearpod
 *
 */
interface IClassroomSettings
{
}
