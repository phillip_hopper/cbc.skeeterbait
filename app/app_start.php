<?php

define('DS', DIRECTORY_SEPARATOR);
define('PROJECT_DIR', dirname(__DIR__));
define('CONFIG_DIR', PROJECT_DIR . DS . 'config');

include_once __DIR__ . DS . 'interfaces.php';
include_once __DIR__ . DS . 'ClassroomSettings.php';
